Background
==============
The company is changing the way it represents the order management in business applications: previously sales managers of the company used to create many *sales opportunities* for each *customer*. Then the project managers of the company had to create a single *sales agreement* for each *sales opportunity*. This model appeared to be inefficient, because in real life it is way more convenient to have many *sales agreements* generated from a single *sales opportunity* (e.g. the company sales team creates a *sales opportunity* to upgrade websites for a *customer*. It would be much better to have different agreements for each website upgrade, but the previous model did not allow that).

Therefore the company decided to have an additional business model for order management supported in the system, which allows one-to-many relationship. Now the sales managers can create *customer contracts* and projects managers can create many *delivery contracts* based on them. (e.g. the company sales team creates a *customer contract* for upgrade of all cusomer websites and the project managers create *delivery contract* for each website mentioned in the *customer contract*).

Since the company still haves existing *sales agreements* that have not been completed yet, the business tools have to support both models.


Provided base implementation
==============
The provided implementation is a functional prototype which imitates a business tool whose purpose is to show order management data in the main page of the website. The tool supports only the older order managent model: it has entities 'SalesAgreement', 'SalesOpportunity' and 'Customer'. Mock sales data is injected via BootStrap.groovy according to domain models and then represented in the client-side grid, defined in DataGrid.js and DataGrid.ui.js. All of the CRUD operations can simply be performed using *grails scaffolding* (http://grails.org/doc/2.3.8/guide/scaffolding.html) feature and accessed using the buttons above the grid in the main page (e.g. button "Edit Customers" opens it).


Requirements for implementation
==============
- The general requirement is to change the tool so it supports both models, described in the **About** section, i.e.:
- The column *Agreement Title* should become *Order Title*, *Agreement Amount* -> *Order Amount*, *Sales Opportunity* -> *Sale*, *Opportunity Currency* -> *Sale Currency*;
- The main client-side grid currently represents only the *sales agreements*. It must show both *sales agreements* and *delivery contracts* in the same grid. The same columns should be used to show the same purpose fields, e.g. the titles of *delivery contract* and *sales agreement* must be under *Order Title* etc.;
- The users should be able to create/update/delete *sales opportunities*, *sales agreements*, *customer contracts* and *delivery contracts* (for client side - it is enough to use *grails scaffolding*). The new entities must use the same **purpose** (different must be applied) fields as the current ones, i.e. *sales opportunities* have the same fields as *customer contracts*, so does the *sales agreements* with *delivery contracts*;
- Mock data to represent one-to-many relationship between *customer contract* and *delivery contracts* must be injected via BootStrap.groovy (or in any other way);
- A new column in the client-side grid should be added, which shows the percentage amount of sales money used for orders (i.e. what % of *sales opportunity* money was used for *sales agreement* and what % of *customer contract* money was used for all *delivery contracts* together);


References
==============
- It is recommended to use 'Groovy/Grails Tool Suite 3.5.1' (http://spring.io/tools/ggts) as your IDE
- The client-side framework is ExtJS 3.4. The documentation can be found at http://docs.sencha.com/extjs/3.4.0/
- The server-side is coded using Grails 2.3.8. The documentation can be found at http://grails.org/doc/2.3.x/


Deliverables
==============
Packaged git repo with implementation and any additional resources you see fit.


Other considerations
==============
The implementation you create should **not be made publicly available** or otherwise shared/distributed to any outside parties.
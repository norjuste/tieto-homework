SalesAgreementAndDeliveryContractStore = Ext.extend(Ext.data.JsonStore, {
    constructor : function(cfg) {
        cfg = cfg || {};
        SalesAgreementAndDeliveryContractStore.superclass.constructor.call(this, Ext.apply({
        	autoLoad : true,
            storeId : 'SalesAgreementAndDeliveryContractStore',
            root: 'data',
            api : {
                read : 'deliveryContractAndSalesAgreement/getAll'
            },
//            idProperty : 'id',
            fields : [ {
                name : 'agreementTitle',
                type : 'string'
            }, {
            	name : 'agreementAmount',
                type : 'number'
            }, {
            	name : 'salesOpportunity',
            	type : 'string'
            }, {
            	name : 'salesOpportunityAmount',
            	type : 'number'
            }, {
            	name : 'salesOpportunityCurrency',
            	type : 'string'
            }, {
            	name : 'customer',
            	type : 'string'
            }, {
            	name : 'percentageAmount',
            	type : 'number'
            }, ]
        }, cfg));
    }
});
new SalesAgreementAndDeliveryContractStore();
DataGridUi = Ext.extend(Ext.grid.GridPanel, {
    stateId: 'DataGrid',
    store: 'SalesAgreementAndDeliveryContractStore',
    flex: 1,
    
    initComponent: function() {
    	this.tbar = {
			xtype: 'toolbar',
			items: [
			    {
			        xtype: 'button',
			        text: 'Edit Customers',
			        handler: function() {
			        	window.location.href = 'customer';
			        }
			    },
			    {
			        xtype: 'button',
			        text: 'Edit Opportunities',
			        handler: function() {
			        	window.location.href = 'salesOpportunity';
			        }
			    },
			    {
			        xtype: 'button',
			        text: 'Edit Agreements',
			        handler: function() {
			        	window.location.href = 'salesAgreement';
			        }
			    },
			    {
			        xtype: 'button',
			        text: 'Edit Customer Contracts',
			        handler: function() {
			        	window.location.href = 'customerContract';
			        }
			    },
			    {
			        xtype: 'button',
			        text: 'Edit Delivery Contracts',
			        handler: function() {
			        	window.location.href = 'deliveryContract';
			        }
			    },
			]
        };
    	
        this.columns = [
            {
				xtype: 'gridcolumn',
				id: 'agreementTitle',
				dataIndex: 'agreementTitle',
				header: 'Order Title',
				sortable: true,
				width: 200
            },
            {
				xtype: 'gridcolumn',
				id: 'agreementAmount',
				dataIndex: 'agreementAmount',
				header: 'Order Amount',
				sortable: true,
				width: 200
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'salesOpportunity',
                header: 'Sale',
                sortable: true,
                width: 350
            },
            {
				xtype: 'gridcolumn',
				id: 'salesOpportunityAmount',
				dataIndex: 'salesOpportunityAmount',
				header: 'Opportunity Amount',
				sortable: true,
				width: 200
            },
            {
				xtype: 'gridcolumn',
				id: 'salesOpportunityCurrency',
				dataIndex: 'salesOpportunityCurrency',
				header: 'Sale Currency',
				sortable: true,
				width: 200
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'customer',
                header: 'Customer',
                sortable: true,
                width: 350
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'percentageAmount',
                header: 'Percentage amount',
                sortable: true,
                width: 350
            }
        ];
        
        DataGridUi.superclass.initComponent.call(this);
    }
});
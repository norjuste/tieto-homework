AppContainerUi = Ext.extend(Ext.Viewport, {
	layout : {
		align: 'stretch',
		pack: 'start',
		type: 'vbox'
	},

	initComponent : function() {
		this.items = [
          {
			xtype: 'datagrid',
			ref: 'datagrid'
          }
        ];
		AppContainerUi.superclass.initComponent.call(this);
	}
});
package tieto.homework

import grails.transaction.Transactional

@Transactional
class DeliveryContractService {
    public List<DeliveryContract> getAllDeliveryContracts() {
		return DeliveryContract.getAll()
    }
}
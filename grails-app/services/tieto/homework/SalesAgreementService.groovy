package tieto.homework

import grails.transaction.Transactional

@Transactional
class SalesAgreementService {
    public List<SalesAgreement> getAllSalesAgreements() {
		return SalesAgreement.getAll()
    }
}
package tieto.homework

import groovy.transform.ToString

@ToString
class SalesAgreement {
	static belongsTo = [salesOpportunity: SalesOpportunity]
	
	String agreementTitle
	BigDecimal agreementAmount
	
    static constraints = {
		agreementTitle unique: true, blank: false, size: 5..50
		salesOpportunity unique: true, nullable: false
    }
	
	public Float getPercentageAmount() {
		this.agreementAmount * 100 / this.salesOpportunity.opportunityAmount
	}
	
	public Map toJsonMap() {
		[
			percentageAmount: this.getPercentageAmount(),
			agreementTitle: this.agreementTitle,
			agreementAmount: this.agreementAmount,
			salesOpportunity: this.salesOpportunity.opportunityName,
			salesOpportunityAmount: this.salesOpportunity.opportunityAmount,
			salesOpportunityCurrency: this.salesOpportunity.opportunityCurrency,
			customer: this.salesOpportunity.customer.clientName
		]
	}
}
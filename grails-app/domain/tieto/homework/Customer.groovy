package tieto.homework

import groovy.transform.ToString

@ToString
class Customer {
	static hasMany = [salesOpportunities: SalesOpportunity, customerContracts: CustomerContract]
	
	String clientName
	String clientURL
	String clientPhoneNumber
	String clientAddress
	
    static constraints = {
		clientName nullable: false, unique: true
		clientURL nullable: true
		clientPhoneNumber nullable: true
		clientAddress nullable: true
    }
}
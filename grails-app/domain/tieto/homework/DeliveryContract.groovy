package tieto.homework

import groovy.transform.ToString

@ToString
class DeliveryContract {
	static belongsTo = [customerContract: CustomerContract]
	
	String contractTitle
	BigDecimal contractAmount
	
    static constraints = {
		contractTitle unique: true, blank: false, size: 5..50
		customerContract unique: false, nullable: false
    }
	
	public Float getPercentageAmount() {
		def deliveryContracts = this.customerContract.deliveryContract
		def sum = 0
		
		for (deliveryContract in deliveryContracts) {
			sum += deliveryContract.contractAmount
		}
		
		sum * 100 / this.customerContract.contractAmount
	}
	
	public Map toJsonMap() {
		[
			percentageAmount: this.getPercentageAmount(),
			agreementTitle: this.contractTitle,
			agreementAmount: this.contractAmount,
			salesOpportunity: this.customerContract.contractName,
			salesOpportunityAmount: this.customerContract.contractAmount,
			salesOpportunityCurrency: this.customerContract.contractCurrency,
			customer: this.customerContract.customer.clientName
		]
	}
}
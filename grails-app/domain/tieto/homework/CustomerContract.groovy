package tieto.homework

import groovy.transform.ToString

@ToString
class CustomerContract {
	static hasOne = [customer: Customer]
	static hasMany = [deliveryContract: DeliveryContract]
	
	String contractName
	BigDecimal contractAmount
	String contractCurrency
	
    static constraints = {
		customer unique: false, nullable: false
		contractName unique: true, blank: false, size: 5..50
		contractAmount nullable: false
		contractCurrency nullable: false
    }
}
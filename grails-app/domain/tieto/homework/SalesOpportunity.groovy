package tieto.homework

import groovy.transform.ToString

@ToString
class SalesOpportunity {
	static hasOne = [salesAgreement: SalesAgreement, customer: Customer]
	
	String opportunityName
	BigDecimal opportunityAmount
	String opportunityCurrency
	
    static constraints = {
		salesAgreement unique: true, nullable: true
		customer unique: false, nullable: false
		opportunityName unique: true, blank: false, size: 5..50
		opportunityAmount nullable: false
		opportunityCurrency nullable: false
    }
}
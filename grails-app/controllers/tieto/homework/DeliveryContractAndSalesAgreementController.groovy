package tieto.homework

import com.sun.org.apache.xml.internal.security.encryption.AgreementMethod;
import grails.converters.JSON

class DeliveryContractAndSalesAgreementController {
	SalesAgreementService salesAgreementService
	DeliveryContractService deliveryContractService
	
	def getAll = {
		def agreements = salesAgreementService.getAllSalesAgreements()
		def contracts = deliveryContractService.getAllDeliveryContracts()
		def agreementsAndContracts = []
		
		agreementsAndContracts.addAll(agreements)
		agreementsAndContracts.addAll(contracts)
		
		render ([data: agreementsAndContracts.collect { it.toJsonMap() }] as JSON)
	}	
}
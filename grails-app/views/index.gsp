<!DOCTYPE html>
<html>
    <head>
        <g:javascript src="ext-3.4.0/adapter/ext/ext-base.js" />
		<g:javascript src="ext-3.4.0/ext-all.js" />
		<title>Tieto homework</title>
		<r:require module="application" />
		<r:layoutResources />
    </head>
    
    <body>
        <r:layoutResources/>
    </body>
</html>
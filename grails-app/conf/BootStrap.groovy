import tieto.homework.Customer
import tieto.homework.CustomerContract
import tieto.homework.DeliveryContract
import tieto.homework.SalesAgreement
import tieto.homework.SalesOpportunity

class BootStrap {

    def init = { servletContext ->
		clearData()
		populateInitialMockData()
    }
	
	def void clearData() {
		SalesAgreement.executeUpdate('delete from SalesAgreement')
		SalesOpportunity.executeUpdate('delete from SalesOpportunity')
		Customer.executeUpdate('delete from Customer')
	}
	
	def void populateInitialMockData() {
		// Populating a few customers
		def client1 = new Customer(
			clientName: 'Real Company JSC',
			clientURL: 'www.client1test.com',
			clientPhoneNumber: '370 698 11111',
			clientAddress: 'Clients str. 1'
		).save(failOnError: true)
		
		def client2 = new Customer(
			clientName: 'Artificial Customer Ltd.',
			clientURL: 'www.client2test.com',
			clientPhoneNumber: '370 698 22222',
			clientAddress: 'Clients str. 2'
		).save(failOnError: true)
		
		// Populating a few sales opportunities
		def salesOpportunity1 = new SalesOpportunity(
			customer: client1,
			opportunityName: 'Great Deal',
			opportunityAmount: 500,
			opportunityCurrency: 'EUR'
		).save(failOnError: true)
		
		def salesOpportunity2 = new SalesOpportunity(
			customer: client1,
			opportunityName: 'VCS Migration Deal',
			opportunityAmount: 20000.89,
			opportunityCurrency: 'EUR'
		).save(failOnError: true)
		
		def salesOpportunity3 = new SalesOpportunity(
			customer: client2,
			opportunityName: 'Hardware Upgrade Deal',
			opportunityAmount: 10000,
			opportunityCurrency: 'LTL'
		).save(failOnError: true)
		
		def salesOpportunity4 = new SalesOpportunity(
			customer: client2,
			opportunityName: 'FB API Integration Inquiry',
			opportunityAmount: 1000,
			opportunityCurrency: 'EUR'
		).save(failOnError: true)
		
		def salesOpportunity5 = new SalesOpportunity(
			customer: client2,
			opportunityName: 'Client 2 - Jenkins Production Setup',
			opportunityAmount: 500,
			opportunityCurrency: 'EUR'
		).save(failOnError: true)
		
		// Populating a few sales agreements
		def agreement1 = new SalesAgreement(
			salesOpportunity: salesOpportunity1,
			agreementTitle: 'Donation to the Open Source',
			agreementAmount: 1000
		).save(failOnError: true)
		
		def agreement2 = new SalesAgreement(
			salesOpportunity: salesOpportunity2,
			agreementTitle: 'BitBucket Yearly Payment',
			agreementAmount: 50
		).save(failOnError: true)
		
		
		def agreement3 = new SalesAgreement(
			salesOpportunity: salesOpportunity3,
			agreementTitle: 'Buying Additional 500GB of HDD',
			agreementAmount: 200
		).save(failOnError: true)
		
		def agreement4 = new SalesAgreement(
			salesOpportunity: salesOpportunity4,
			agreementTitle: 'API Buying',
			agreementAmount: 10
		).save(failOnError: true)
		
		// Populating a few customer contracts
		def customerContract1 = new CustomerContract(
			customer: client1,
			contractName: 'Great Deal (cc)',
			contractAmount: 500,
			contractCurrency: 'EUR'
		).save(failOnError: true)
		
		def customerContract2 = new CustomerContract(
			customer: client1,
			contractName: 'VCS Migration Deal (cc)',
			contractAmount: 20000.89,
			contractCurrency: 'EUR'
		).save(failOnError: true)
		
		def customerContract3 = new CustomerContract(
			customer: client2,
			contractName: 'Hardware Upgrade Deal (cc)',
			contractAmount: 10000,
			contractCurrency: 'LTL'
		).save(failOnError: true)
		
		def customerContract4 = new CustomerContract(
			customer: client2,
			contractName: 'FB API Integration Inquiry (cc)',
			contractAmount: 1000,
			contractCurrency: 'EUR'
		).save(failOnError: true)
		
		def customerContract5 = new CustomerContract(
			customer: client2,
			contractName: 'Client 2 - Jenkins Production Setup (cc)',
			contractAmount: 500,
			contractCurrency: 'EUR'
		).save(failOnError: true)
		
		// Populating a few delivery contracts
		def deliveryContract1 = new DeliveryContract(
			customerContract: customerContract1,
			contractTitle: 'Donation to the Open Source (dc)',
			contractAmount: 1000
		).save(failOnError: true)
		
		def deliveryContract2 = new DeliveryContract(
			customerContract: customerContract1,
			contractTitle: 'BitBucket Yearly Payment (dc)',
			contractAmount: 50
		).save(failOnError: true)
		
		
		def deliveryContract3 = new DeliveryContract(
			customerContract: customerContract2,
			contractTitle: 'Buying Additional 500GB of HDD (dc)',
			contractAmount: 200
		).save(failOnError: true)
		
		def deliveryContract4 = new DeliveryContract(
			customerContract: customerContract3,
			contractTitle: 'API Buying (dc)',
			contractAmount: 10
		).save(failOnError: true)
	}
}
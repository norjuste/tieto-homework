import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH

modules = {
    application {
		resource url:'css/ext-all.css'
		
        resource url:'js/application.js'
		resource url:'js/store/SalesAgreementAndDeliveryContractStore.js'
		resource url:'js/ui/AppContainer.ui.js'
		resource url:'js/ui/DataGrid.ui.js'
		resource url:'js/AppContainer.js'
		resource url:'js/DataGrid.js'
		resource url:'js/main.js'
    }
}